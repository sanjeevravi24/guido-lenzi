insert into  public.compilado_mensal
select * from(
SELECT 
	"PERIOD", "Item", "BRAND", "KPI", "LINHA", "GrupoProduto", "Categoria", 
	"Subcategoria", "Subcategoria2", "Capacidade", "Tipo", "Acabamento", "FirstActivityMonthly",
	SUM("VALOR")
FROM public.compilado_mensal_temp
GROUP BY
	"PERIOD", "Item", "BRAND", "KPI", "LINHA", "GrupoProduto", "Categoria", 
	"Subcategoria", "Subcategoria2", "Capacidade", "Tipo", "Acabamento", "FirstActivityMonthly"
except 
SELECT 
	"period", item, brand, kpi, linha, grupoproduto, categoria,
	subcategoria, subcategoria2, capacidade, tipo, acabamento, firstactivitymonthly, 
	SUM(valor)
FROM public.compilado_mensal
GROUP BY
	"period", item, brand, kpi, linha, grupoproduto, categoria,
	subcategoria, subcategoria2, capacidade, tipo, acabamento, firstactivitymonthly) inserir_compilado
;

select * from(
SELECT 
	"PERIOD", "Item", "BRAND", "KPI", "LINHA", "GrupoProduto", "Categoria", 
	"Subcategoria", "Subcategoria2", "Capacidade", "Tipo", "Acabamento", "FirstActivityMonthly",
	SUM("VALOR")
FROM public.compilado_mensal_temp
GROUP BY
	"PERIOD", "Item", "BRAND", "KPI", "LINHA", "GrupoProduto", "Categoria", 
	"Subcategoria", "Subcategoria2", "Capacidade", "Tipo", "Acabamento", "FirstActivityMonthly"
except 
SELECT 
	"period", item, brand, kpi, linha, grupoproduto, categoria,
	subcategoria, subcategoria2, capacidade, tipo, acabamento, firstactivitymonthly, 
	SUM(valor)
FROM public.compilado_mensal
GROUP BY
	"period", item, brand, kpi, linha, grupoproduto, categoria,
	subcategoria, subcategoria2, capacidade, tipo, acabamento, firstactivitymonthly) inserir_compilado
;

/*
 * mostrar duplicidade strings
 * mas valores diferentes
 */
select 
	"PERIOD", "Item", "BRAND", "KPI", "LINHA", "GrupoProduto", "Categoria", 
	"Subcategoria", "Subcategoria2", "Capacidade", "Tipo", "Acabamento", "FirstActivityMonthly","VALOR",
	count(*)
from compilado_mensal_temp
group by
	"PERIOD", "Item", "BRAND", "KPI", "LINHA", "GrupoProduto", "Categoria", 
	"Subcategoria", "Subcategoria2", "Capacidade", "Tipo", "Acabamento", "FirstActivityMonthly","VALOR" 
order by 
	count(*) desc
;
