CREATE TABLE IF NOT EXISTS compiladobrands_semanal (
	"csv_id" VARCHAR(500) NOT NULL,
    "period" VARCHAR(500),
    brand VARCHAR(500),
    distributio_typ VARCHAR(500),
    kpi VARCHAR(500),
    linha VARCHAR(500),
    grupoproduto VARCHAR(500),
    categoria VARCHAR(500),
    subcategoria VARCHAR(500),
    capacidade VARCHAR(500),
    tipo VARCHAR(500),
    valor VARCHAR(500),
    FOREIGN KEY ("csv_id")
        REFERENCES elux ("csv_id"));

