-- create json csv table
CREATE TABLE IF NOT EXISTS elux (
    productgroup VARCHAR(20),
    sector VARCHAR(3),
    "file" VARCHAR(255) ,
    "csv_id" VARCHAR(255)  PRIMARY KEY,
    filedate VARCHAR(255),
    "period" VARCHAR(255),
    typefile VARCHAR(20),
    typeperiod VARCHAR(7),
    download Boolean default false,
    inserted_first_date timestamp default CURRENT_TIMESTAMP,
    updated_datetime timestamp default CURRENT_TIMESTAMP
    );

