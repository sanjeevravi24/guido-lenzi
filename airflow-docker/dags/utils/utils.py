import csv

import psycopg2
# import datetime
import requests
import logging
import json
import io

import pandas as pd

from sqlalchemy import create_engine
from contextlib import closing

url_download = 'http://www.gfkcr.com.br:1024/gfk_elux_download/'
user, password = 'elux', 'Ap7Electr@lux_2@2I'

HOST = 'postgres_db'
POSTGRESS_USER = 'postgres'
POSTGRESS_PASSWORD = 'postgres'
DATABASE = 'postgres'

ENGINE = create_engine(f"postgresql://{POSTGRESS_USER}:{POSTGRESS_PASSWORD}@{HOST}/{DATABASE}")


def download_json_df(url_query, user, password):
    resp = requests.get(url_query, auth=(user, password))
    return pd.DataFrame(json.loads(resp.text))


def clean_df_headers(df):
    df.columns = map(str.lower, df.columns)
    df.columns = df.columns.str.replace(' ', '_')
    df.columns = df.columns.str.replace('.', '')
    df.columns = df.columns.str.replace('%', '_pct')
    return df


def read_sql(typefile, typeperiod):
    logging.warn('read_sql')
    return pd.read_sql_query(
        'select * from "elux" '
        'where '
        f"typefile = '{typefile.upper()}' and "
        f"typeperiod = '{typeperiod.upper()}' and download = false ",
        con=ENGINE
    )


def bring_csv_to_memory(filename):
    url = f'{url_download}{filename}'
    req = requests.get(url, auth=(user, password))
    return req


def memory_csv_to_df(req):
    s = req.content
    df = pd.read_csv(io.StringIO(s.decode('latin1')),
                     encoding='latin1',
                     sep=';',
                     decimal=',',
                     )
    return clean_df(df)


def clean_df(df):
    df.columns = map(str.lower, df.columns)
    df.columns = df.columns.str.replace(' ', '_')
    df.columns = df.columns.str.replace('.', '')
    df.columns = df.columns.str.replace('-', '_')
    df.columns = df.columns.str.replace('%', '_pct')
    df.drop_duplicates(inplace=True)
    return df


def save_to_file(df, typefile, typeperiod):
    conn = psycopg2.connect(
        dbname=DATABASE,
        user=POSTGRESS_USER,
        password=POSTGRESS_PASSWORD,
        host=HOST
    )
    df.to_sql(f"{typefile}_{typeperiod}",
              ENGINE,
              if_exists='append',
              index=False)
    with closing(conn.cursor()) as curs:
        curs.execute(
            f"update elux set download=true, updated_datetime = CURRENT_TIMESTAMP where csv_id  IN {tuple(df['csv_id'].unique().tolist() + [''] )}"
        )
        conn.commit()


def delete_data(column_list):
    conn = psycopg2.connect(
        dbname=DATABASE,
        user=POSTGRESS_USER,
        password=POSTGRESS_PASSWORD,
        host=HOST
    )

    with closing(conn.cursor()) as curs:
        curs.execute(
            f'delete from public.compilado_mensal where csv_id not in {column_list};'
            f'delete from public.compilado_semanal where csv_id not in {column_list};'
            f'delete from public.compiladobrands_mensal where csv_id not in {column_list};'
            f'delete from public.compiladobrands_semanal where csv_id not in {column_list};'
            f'delete from public.elux where csv_id not in {column_list};'
        )


def truncate_temp(df, typefile, typeperiod):
    conn = psycopg2.connect(
        dbname=DATABASE,
        user=POSTGRESS_USER,
        password=POSTGRESS_PASSWORD,
        host=HOST
    )

    with closing(conn.cursor()) as curs:
        curs.execute(
            f'truncate public.{typefile}_{typeperiod} '
        )


def download_csv(x, typefile, typeperiod):
    logging.warn(f'download_csv, filename:{x.csv_id}')
    req = bring_csv_to_memory(x.file)
    logging.warn('request')
    if req.status_code == 200:
        df = memory_csv_to_df(req)
        alter_table(df, typefile, typeperiod)
        df.insert(0, 'csv_id', x.csv_id)
        logging.warn(x.csv_id)
        save_to_file(df, typefile, typeperiod)
    else:
        logging.warn('request url incorrect or api is down')


def alter_table(df, typefile, typeperiod):
    conn = psycopg2.connect(
        dbname=DATABASE,
        user=POSTGRESS_USER,
        password=POSTGRESS_PASSWORD,
        host=HOST
    )
    with closing(conn.cursor()) as curs:
        for column_name, data_type in df.dtypes.to_dict().items():
            print(f"ALTER TABLE public.{typefile}_{typeperiod} ADD COLUMN IF NOT EXISTS {column_name} VARCHAR(500)")
            curs.execute(
                f'ALTER TABLE public.{typefile}_{typeperiod} ADD COLUMN IF NOT EXISTS {column_name} VARCHAR(500)'
            )
        conn.commit()


def check_updates(df):
    items = df.to_dict('records')
    conn = psycopg2.connect(
        dbname=DATABASE,
        user=POSTGRESS_USER,
        password=POSTGRESS_PASSWORD,
        host=HOST
    )
    with closing(conn.cursor()) as curs:
        for item in items:
            query = "select csv_id from elux where csv_id = '{}' and period = '{}' and filedate != '{}'".format(
                item["csv_id"], item["period"], item["filedate"])
            curs.execute(query)
            changed_csv_id = curs.fetchone()
            if changed_csv_id:
                changed_csv_id = changed_csv_id[0]
                print(f"delete from public.compiladobrands_semanal where csv_id  = '{changed_csv_id}';")
                curs.execute(
                    f"delete from public.compilado_mensal where csv_id  = '{changed_csv_id}';"
                    f"delete from public.compilado_semanal where csv_id  = '{changed_csv_id}';"
                    f"delete from public.compiladobrands_mensal where csv_id  = '{changed_csv_id}';"
                    f"delete from public.compiladobrands_semanal where csv_id  = '{changed_csv_id}';"
                    f"update elux set download=false, updated_datetime = CURRENT_TIMESTAMP,filedate=" + "'" + item["filedate"] + "'" + "where csv_id = " + "'" + changed_csv_id + "'"
                )
            conn.commit()
