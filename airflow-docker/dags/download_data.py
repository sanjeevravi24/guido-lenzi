import pandas as pd
import datetime
import hashlib
from airflow import DAG
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.operators.python import PythonOperator
from sqlalchemy import create_engine
from utils.utils import download_json_df, clean_df_headers, read_sql, download_csv, delete_data, check_updates

url_query = 'http://www.gfkcr.com.br:1024/gfk_elux_files'
url_download = 'http://www.gfkcr.com.br:1024/gfk_elux_download/'
user, password = 'elux', 'Ap7Electr@lux_2@2I'

HOST = 'postgres_db'
POSTGRESS_USER = 'postgres'
POSTGRESS_PASSWORD = 'postgres'
DATABASE = 'postgres'
ENGINE = create_engine(f"postgresql://{POSTGRESS_USER}:{POSTGRESS_PASSWORD}@{HOST}/{DATABASE}")


def download_elux():
    TABLE = 'elux'
    df = download_json_df(url_query, user, password)
    df = clean_df_headers(df)
    df['csv_id'] = df.apply(lambda x: hashlib.md5(str(x['file'] + x['period']).encode()).hexdigest(), axis=1)
    df2 = df.copy()
    temp_df = pd.read_sql_query("select csv_id from elux", con=ENGINE)
    temp_values = [list(i) for i in zip(*temp_df.values)]
    if temp_values:
        df = df[~df.csv_id.isin(temp_values[0])]
        delete_csv_df = temp_df[~temp_df.csv_id.isin(df['csv_id'].unique().tolist())]
        delete_csv = tuple(delete_csv_df['csv_id'].unique().tolist())
        if delete_csv:
            delete_data(delete_csv)
    df.to_sql(TABLE, ENGINE, if_exists='append', index=False)
    check_updates(df2)

def download_tables(typefile, typeperiod):
    df = read_sql(typefile, typeperiod)
    df.apply(lambda x: download_csv(x, typefile, typeperiod), axis=1)


with DAG(
        dag_id="download_data",
        start_date=datetime.datetime(2020, 2, 2),
        schedule_interval="@daily",
        catchup=False,
        tags=['apipeline']
) as dag:
    create_elux_table = PostgresOperator(
        task_id="create_elux_table",
        postgres_conn_id="postgres_default",
        sql="sql/schemas/elux_schema.sql",
    )

    create_compilado_mensal_table = PostgresOperator(
        task_id="create_compilado_mensal_table",
        postgres_conn_id="postgres_default",
        sql="sql/schemas/compilado_mensal_schema.sql",
    )

    create_compilado_semanal_table = PostgresOperator(
        task_id="create_compilado_semanal_table",
        postgres_conn_id="postgres_default",
        sql="sql/schemas/compilado_semanal_schema.sql",
    )

    create_compiladobrands_mensal_table = PostgresOperator(
        task_id="create_compiladobrands_mensal_table",
        postgres_conn_id="postgres_default",
        sql="sql/schemas/compiladobrands_mensal_schema.sql",
    )

    create_compiladobrands_semanal_table = PostgresOperator(
        task_id="create_compiladobrands_semanal_table",
        postgres_conn_id="postgres_default",
        sql="sql/schemas/compiladobrands_semanal_schema.sql",
    )

    download_elux_task = PythonOperator(
        task_id='download_elux',
        python_callable=download_elux,
    )

    create_elux_table >> [
        create_compilado_mensal_table,
        create_compilado_semanal_table,
        create_compiladobrands_mensal_table,
        create_compiladobrands_semanal_table
    ]
    create_elux_table >> download_elux_task
    for typefile in ['compilado', 'compiladobrands']:
        for typeperiod in ['semanal', 'mensal']:
            download = PythonOperator(
                task_id=f'download_{typefile}_{typeperiod}',
                python_callable=download_tables,
                op_kwargs={'typefile': typefile, 'typeperiod': typeperiod}
            )
            download_elux_task >> download
