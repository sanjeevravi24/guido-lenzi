# guido-lenzi

# How to run

```bash
git clone https://gitlab.com/sanjeevravi24/guido-lenzi
cd guido-lenzi/airflow-docker
mkdir ./dags ./plugins ./logs
echo -e "AIRFLOW_UID=$(id -u)\nAIRFLOW_GID=0" > .env
sudo docker-compose up airflow-init
sudo docker-compose up
```
