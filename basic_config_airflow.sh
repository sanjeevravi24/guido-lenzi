# source: https://www.youtube.com/watch?v=aTaytcxy2Ck
# AIRFLOW__CORE__LOAD_EXAMPLES: 'false'

mkdir airflow-docker
cd airflow-docker
curl -LfO 'https://airflow.apache.org/docs/apache-airflow/stable/docker-compose.yaml'
mkdir ./dags ./plugins ./logs
echo -e "AIRFLOW_UID=$(id -u)\nAIRFLOW_GID=0" > .env
sudo docker-compose airflow-init